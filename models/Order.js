const mongoose = require('mongoose');

const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "User ID is required"]
        },
        productId: {
            type: String,
            required: [true, "Product ID required"],
        },
        
        quantity: {
            type: Number,
            default: 0,
        },

        totalAmount:{
        type: Number,
        default: 0.00
        },

        purchasedOn:{
       type: Date,
        default: new Date()
        }
})

module.exports = mongoose.model("Order", orderSchema)
