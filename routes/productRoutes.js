const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');


router.post("/", auth.verify, (req, res) => {

	const isAdminData = auth.decode(req.headers.authorization).isAdmin;
	console.log(isAdminData)
	productController.createProduct(req.body, isAdminData).then(resultFromController => res.send(resultFromController))
});


router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization)


	productController.getAllProducts(userData).then(resultFromController => res.send(resultFromController))
});


router.get("/", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
});


router.get("/:productId", (req, res) => {
	productController.getSingleProduct(req.params).then(resultFromController => res.send(resultFromController))
});


router.put("/:productId", auth.verify, (req, res) => {

	const data = auth.decode(req.headers.authorization)
	productController.updateProduct(req.params, req.body, data).then(resultFromController => res.send(resultFromController))
});


router.put("/archive/:productId", auth.verify, (req, res) => {

	const data = {
		productId: req.params.productId,
		payload: auth.decode(req.headers.authorization).isAdmin
	}
	console.log(data)

	productController.archiveProduct(data).then(resultFromController => res.send(resultFromController))
});


router.put('/activateProduct/:productId', auth.verify, (req, res) => {

	const data = {
		productId : req.params.productId,
		isAdmin : auth.decode(req.headers.authorization).isAdmin
	}

	productController.activateProduct(data).then(resultFromController => res.send(resultFromController))
});

module.exports = router;
